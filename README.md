# bfu-semantic-web-course

A coursework for uni. Represents the Roman Hierarchy system in a simple manner. The ontology is created with Protege 5 and the whole ontology is described in Bulgarian language.

![Roman society ontology](ontology-roman-society.png)
